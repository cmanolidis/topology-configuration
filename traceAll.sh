echo ""
echo ""
echo "Node: $(hostname -s)"

for i in {1..12}
do
    IPaddr=192.168."$i".1
    if ! [[ $(echo $(ifconfig br0 | grep -Eo 'inet (addr:)?([0-9]*\.){3}[0-9]*' | grep -Eo '([0-9]*\.){3}[0-9]*') | cut -d"." -f1-4) == $IPaddr ]]; then
        MACaddr=$(batctl translate $IPaddr)
        echo "MAC address of $IPaddr: $MACaddr"
        batctl traceroute $MACaddr
        echo ""
    fi
done
