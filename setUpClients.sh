#load and run scripts on each client
for dest in $(<clientList.txt); do
    scp -o "StrictHostKeyChecking no" client.sh $"root@"${dest}:/root/
    ssh -o "StrictHostKeyChecking no" $"root@"${dest} "bash client.sh"
done
