######################## ADHOC ########################
#apt-get -y install git dnsmasq hostapd bridge-utils


APnet=255.255.255.0

#Wi-Fi AP and ADHOC interface name.
ifADHOC=wlan1
ifAP=ap0
#essid name and channel for B.A.T.M.A.N. ad-hoc  nodes.
ESSID=adhoc-test
CHANNEL=4
#Cell ID of ad-hoc nodes.
CELLID=02:12:34:56:78:9A

declare -A AccesPointIPs

#########################################################
###### Configuration of the topology that we using ######
#########################################################

#Define the IPs for each node's Access Point subnet.
AccesPointIPs["node19-2"]=192.168.1.1
AccesPointIPs["node10-1"]=192.168.2.1
AccesPointIPs["node3-3"]=192.168.3.1
AccesPointIPs["node20-10"]=192.168.4.1
AccesPointIPs["node13-8"]=192.168.5.1
AccesPointIPs["node8-8"]=192.168.6.1
AccesPointIPs["node13-13"]=192.168.7.1
AccesPointIPs["node6-15"]=192.168.8.1
AccesPointIPs["node1-12"]=192.168.9.1
AccesPointIPs["node19-19"]=192.168.10.1
AccesPointIPs["node10-17"]=192.168.11.1
AccesPointIPs["node2-19"]=192.168.12.1
#echo ${AccesPointIPs[*]}
#########################################################

#Find out which node is this.
HOST=$(hostname -s)
apIP=$(echo ${AccesPointIPs["$HOST"]})

modprobe ath5k
ifconfig $ifADHOC up
ifconfig $ifADHOC down
ifconfig $ifADHOC mtu 1532
iwconfig $ifADHOC mode ad-hoc essid $ESSID ap $CELLID channel $CHANNEL
modprobe batman-adv debug=2
batctl if add $ifADHOC
ifconfig $ifADHOC up
sysctl -w net.ipv4.ip_forward=1

ifconfig $ifAP 0.0.0.0

ip link add name br0 type bridge
ip link set dev $ifAP master br0
ip link set dev bat0 master br0

ip link set up dev $ifAP
ip link set up dev bat0
ip link set up dev br0

ip addr replace dev br0 $apIP/24

#batctl bl 1
#batctl ap 1

######################## ROUTING ########################
#ifconfig bat0 $apIP netmask $APnet

SUBNET=$(echo ${apIP} | cut -d"." -f1-3)
route del -net $SUBNET".0" netmask 255.255.255.0 br0
route add -net 192.168.0.0 netmask 255.255.0.0 br0

echo "$HOST is ready."

