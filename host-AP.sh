#Wireless key to be used
KEY=SECRETPASSWORD

modprobe ath9k
#Check if AP is already setup.
if ! [[ $(echo $(ifconfig ap0 | grep -Eo 'inet (addr:)?([0-9]*\.){3}[0-9]*' | grep -Eo '([0-9]*\.){3}[0-9]*') | cut -d"." -f1-2) == "192.168" ]]; then

    essid=$(hostname -s)

    echo "Host Access Point on $essid"
    ./create_ap/create_ap -n wlan0 "$essid" "$KEY" &

else
    echo "$(hostname -s) already setup"
fi
