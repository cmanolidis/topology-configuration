HOSTS="node19-2 node10-1 node3-3 node20-10 node13-8 node8-8 node13-13 node6-15 node1-12 node19-19 node10-17 node2-19"

for host in $HOSTS; do
    echo "Node: $host"
    ssh -o "StrictHostKeyChecking no" $"root@"${host} "batctl originators"
    echo ""
    echo ""
done
