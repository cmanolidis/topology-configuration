
NETMASK=255.255.255.0
#Wi-Fi interface name.
IFACE=wlan0
#The wireless key for the Access Point that will be used.
KEY=SECRETPASSWORD


declare -A ipARRAY
declare -A essidARRAY

#########################################################
###### Configuration of the topology that we using ######
#########################################################
#Define IPs for each node.

ipARRAY["node20-1"]=192.168.1.2
ipARRAY["node19-1"]=192.168.1.3
ipARRAY["node16-1"]=192.168.1.4
ipARRAY["node20-2"]=192.168.1.5
ipARRAY["node18-2"]=192.168.1.6
ipARRAY["node17-2"]=192.168.1.7
ipARRAY["node20-3"]=192.168.1.8
ipARRAY["node19-3"]=192.168.1.9
ipARRAY["node18-3"]=192.168.1.10
ipARRAY["node20-4"]=192.168.1.11
ipARRAY["node19-4"]=192.168.1.12
ipARRAY["node20-5"]=192.168.1.13
ipARRAY["node16-5"]=192.168.1.14

ipARRAY["node12-1"]=192.168.2.2
ipARRAY["node11-1"]=192.168.2.3
ipARRAY["node9-1"]=192.168.2.4
ipARRAY["node8-1"]=192.168.2.5
ipARRAY["node13-3"]=192.168.2.6
ipARRAY["node8-3"]=192.168.2.7
ipARRAY["node11-4"]=192.168.2.8
ipARRAY["node10-4"]=192.168.2.9

ipARRAY["node4-1"]=192.168.3.2
ipARRAY["node2-1"]=192.168.3.3
ipARRAY["node1-1"]=192.168.3.4
ipARRAY["node3-2"]=192.168.3.5
ipARRAY["node2-2"]=192.168.3.6
ipARRAY["node1-2"]=192.168.3.7
ipARRAY["node4-3"]=192.168.3.8
ipARRAY["node1-3"]=192.168.3.9
ipARRAY["node1-5"]=192.168.3.10

ipARRAY["node20-8"]=192.168.4.2
ipARRAY["node18-8"]=192.168.4.3
ipARRAY["node17-10"]=192.168.4.4
ipARRAY["node17-11"]=192.168.4.5
ipARRAY["node20-12"]=192.168.4.6
ipARRAY["node18-13"]=192.168.4.7

ipARRAY["node14-7"]=192.168.5.2
ipARRAY["node13-7"]=192.168.5.3
ipARRAY["node14-8"]=192.168.5.4
ipARRAY["node14-10"]=192.168.5.5
ipARRAY["node14-11"]=192.168.5.6

ipARRAY["node6-6"]=192.168.6.2
ipARRAY["node10-7"]=192.168.6.3
ipARRAY["node9-7"]=192.168.6.4
ipARRAY["node8-7"]=192.168.6.5
ipARRAY["node7-7"]=192.168.6.6
ipARRAY["node7-10"]=192.168.6.7

ipARRAY["node14-14"]=192.168.7.2
ipARRAY["node13-14"]=192.168.7.3
ipARRAY["node12-14"]=192.168.7.4
ipARRAY["node11-14"]=192.168.7.5
ipARRAY["node15-15"]=192.168.7.6

ipARRAY["node8-13"]=192.168.8.2
ipARRAY["node7-13"]=192.168.8.3
ipARRAY["node8-14"]=192.168.8.4
ipARRAY["node7-14"]=192.168.8.5
ipARRAY["node5-16"]=192.168.8.6

ipARRAY["node4-10"]=192.168.9.2
ipARRAY["node1-10"]=192.168.9.3
ipARRAY["node4-11"]=192.168.9.4
ipARRAY["node1-11"]=192.168.9.5
ipARRAY["node3-13"]=192.168.9.6
ipARRAY["node1-13"]=192.168.9.7
ipARRAY["node1-14"]=192.168.9.8

ipARRAY["node20-16"]=192.168.10.2
ipARRAY["node20-17"]=192.168.10.3
ipARRAY["node20-18"]=192.168.10.4
ipARRAY["node18-18"]=192.168.10.5
ipARRAY["node20-19"]=192.168.10.6
ipARRAY["node18-19"]=192.168.10.7
ipARRAY["node20-20"]=192.168.10.8
ipARRAY["node19-20"]=192.168.10.9

ipARRAY["node11-7"]=192.168.11.2
ipARRAY["node13-18"]=192.168.11.3
ipARRAY["node8-18"]=192.168.11.4
ipARRAY["node11-20"]=192.168.11.5
ipARRAY["node10-20"]=192.168.11.6
ipARRAY["node9-20"]=192.168.11.7
ipARRAY["node8-20"]=192.168.11.8

ipARRAY["node3-18"]=192.168.12.2
ipARRAY["node1-18"]=192.168.12.3
ipARRAY["node4-19"]=192.168.12.4
ipARRAY["node3-19"]=192.168.12.5
ipARRAY["node1-19"]=192.168.12.6
ipARRAY["node2-20"]=192.168.12.7
ipARRAY["node1-20"]=192.168.12.8

#echo ${ipARRAY[*]}

#Define the AP essid of each subnetwork.
essidARRAY["192.168.1"]=node19-2
essidARRAY["192.168.2"]=node10-1
essidARRAY["192.168.3"]=node3-3
essidARRAY["192.168.4"]=node20-10
essidARRAY["192.168.5"]=node13-8
essidARRAY["192.168.6"]=node8-8
essidARRAY["192.168.7"]=node13-13
essidARRAY["192.168.8"]=node6-15
essidARRAY["192.168.9"]=node1-12
essidARRAY["192.168.10"]=node19-19
essidARRAY["192.168.11"]=node10-17
essidARRAY["192.168.12"]=node2-19
#########################################################





#Find out what node this is.
HOST=$(hostname -s)

#Get the IP of this node.
IP=$(echo ${ipARRAY["$HOST"]})

#Get the subnet ip of this node.
SUBNET=$(echo ${ipARRAY["$HOST"]} | cut -d"." -f1-3)

#Get the essid for the corresponding AP of this node.
APessid=$(echo ${essidARRAY["$SUBNET"]})

#Get the ip of this subnet's the gateway.
GATEWAY=$SUBNET".1"

#Choose the appropriate wireless card for this node.
modprobe ath9k
ARG=$(echo $(ifconfig $IFACE) | awk '{print$1;}')
if ! [ "$ARG" == $IFACE ]; then
    echo "ath9k does not supported we use: ath5k"
    modprobe ath5k
fi


#IFSUB=$(ifconfig $IFACE | grep -Eo 'inet (addr:)?([0-9]*\.){3}[0-9]*' | grep -Eo '([0-9]*\.){3}[0-9]*')

#Verify that node is not already setup and setup node.
if ! [[ $(echo $(ifconfig $IFACE | grep -Eo 'inet (addr:)?([0-9]*\.){3}[0-9]*' | grep -Eo '([0-9]*\.){3}[0-9]*') | cut -d"." -f1-3) == $SUBNET ]]; then

    echo "IP $IP will be assigned to $HOST."
    echo "The gateway of subnet $SUBNET".0" is: $APessid"

    #Setting up node's IP and connecting to subnet's AP.
    ifconfig $IFACE $IP netmask $NETMASK up
    wpa_passphrase $APessid $KEY > wpa.conf
    wpa_supplicant -i$IFACE -cwpa.conf -B
    #sleep 5

    #Seting up gateway
#    route add -net $SUBNET".0"/24 gw $SUBNET".1" dev $IFACE

    route add -net 192.168.0.0/16 gw $SUBNET".1" dev $IFACE
    route del -net $SUBNET".0" netmask 255.255.255.0 $IFACE

else
    echo "$HOST already configured"
fi

echo " "


