HOSTS="root@node19-2 root@node10-1 root@node3-3 root@node20-10 root@node13-8 root@node8-8 root@node13-13 root@node6-15 root@node1-12 root@node19-19 root@node10-17 root@node2-19"

#run scripts to hosts
for host in $HOSTS; do
    ssh -o "StrictHostKeyChecking no" ${host} "bash adhoc-bat.sh"
done
