
#Check if repository is already downloaded and download it.
if ! [ -d "create_ap" ]; then

    modprobe ath9k

    #Load the proper packages
    apt-get update
    apt-get -y install git dnsmasq hostapd bridge-utils arp-scan

    ifconfig wlan0 up

    #Downoad repository.
    while ! [ -d "create_ap" ]; do
        sudo git clone https://github.com/oblique/create_ap.git
    done

    cd create_ap
    make
fi

#./create_ap/create_ap -n wlan0 "$(hostname -s)" SECRETPASSWORD &
