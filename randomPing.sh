while :
do 
    rndNET=$(shuf -i 1-12 -n 1)
    IPaddr=192.168."$rndNET".1
    if ! [[ $(echo $(ifconfig br0 | grep -Eo 'inet (addr:)?([0-9]*\.){3}[0-9]*' | grep -Eo '([0-9]*\.){3}[0-9]*') | cut -d"." -f1-4) == $IPaddr ]]; then
        batctl ping $IPaddr -c 1
    fi
done
